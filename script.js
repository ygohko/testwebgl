/**
 * Get the normalized vector
 * @param {Array} vector Vector to normalize
 * @returns {Array} Normalized vector
 */
function getNormalizedVector(vector) {
	var length = Math.sqrt(vector[0] * vector[0] + vector[1] * vector[1] + vector[2] * vector[2]);
	var result = [vector[0] / length, vector[1] / length, vector[2] / length];

	return result;
}

/**
 * Get the translation matrix
 * @param {number} x X component of translation
 * @param {number} y Y component of translation
 * @param {number} z Z component of translation
 * @returns {Array} Translation matrix
 */
function getTranslationMatrix(x, y, z) {
	var matrix = [
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		x, y, z, 1.0,
	];

	return matrix;
}

/**
 * Get the Y axis rotation matrix
 * @param {number} degree Degree of rotation angle
 * @returns {Array} Rotation matrix
 */
function getRotationMatrixY(degree) {
	var angle = degree / 360.0 * 2.0 * Math.PI;
	var sine = Math.sin(angle);
	var cosine = Math.cos(angle);
	var matrix = [
		cosine, 0.0, -sine, 0.0,
		0.0, 1.0, 0.0, 0.0,
		sine, 0.0, cosine, 0.0,
		0.0, 0.0, 0.0, 1.0,
	];

	return matrix;
}

/**
 * Get the Z axis rotation matrix
 * @param {number} degree Degree of rotation angle
 * @returns {Array} Rotation matrix
 */
function getRotationMatrixZ(degree) {
	var angle = degree / 360.0 * 2.0 * Math.PI;
	var sine = Math.sin(angle);
	var cosine = Math.cos(angle);
	var matrix = [
		cosine, sine, 0.0, 0.0,
		-sine, cosine, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0,
	];

	return matrix;
}

/**
 * Get the perspective matrix
 * @param {number} fovY Y component of field of view
 * @param {number} aspect Aspect ratio
 * @param {number} nearZ Z position of near clip
 * @param {number} farZ Z position on far clip
 * @returns {Array} Perspective matrix
 */
function getPerspectiveMatrix(fovY, aspect, nearZ, farZ) {
	var matrix = [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
	];

	var angle = (fovY / 2.0 * Math.PI / 180.0);
	var deltaZ = farZ - nearZ;
	var sine = Math.sin(angle);
	if (deltaZ == 0.0 || sine == 0.0 || aspect == 0.0) {
		return matrix;
	}
	var cotangent = Math.cos(angle) / sine;

	matrix[0 * 4 + 0] = cotangent / aspect;
	matrix[1 * 4 + 1] = cotangent;
	matrix[2 * 4 + 2] = -(farZ + nearZ) / deltaZ;
	matrix[2 * 4 + 3] = -1.0;
	matrix[3 * 4 + 2] = -2.0 * nearZ * farZ / deltaZ;
	matrix[3 * 4 + 3] = 0.0;

	return matrix;
}

/**
 * Get the multiplied matrix
 * @param {Array} aMatrix Matrix A
 * @param {Array} bMatrix Matrix B
 * @returns {Array} Multiplied matrix
 */
function getMultipliedMatrix(aMatrix, bMatrix) {
	var matrix = [
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0, 0.0,
	];

	for (var i = 0;i < 4;i++) {
		for (var j = 0;j < 4;j++) {
			var value = 0.0;
			for (var k = 0;k < 4;k++) {
				value += aMatrix[k * 4 + i] * bMatrix[j * 4 + k];
			}
			matrix[j * 4 + i] = value;
		}
	}

	return matrix;
}

/**
 * Class for context
 */
class Context {
	/**
	 * Construct the context
	 * @construct
	 * @param {Object} gl GL context
	 */
	constructor(gl) {
		this._gl = gl;
		this._program = null;
		this._currentMatrix = null;
	}

	/**
	 * Create the shader from id
	 * @param {string} id ID for shader source
	 * @returns {Object} Shader
	 */
	createShader(id) {
		// Get the element
		var element = document.getElementById(id);
		if (!element) {
			return null;
		}

		// Create the shader
		var gl = this._gl;
		var shader = null;
		switch (element.type) {
		case 'x-shader/x-vertex':
			shader = gl.createShader(gl.VERTEX_SHADER);
			break;

		case 'x-shader/x-fragment':
			shader = gl.createShader(gl.FRAGMENT_SHADER);
			break;
		}
		if (!shader) {
			return null;
		}

		// Compile the shader
		gl.shaderSource(shader, element.text);
		gl.compileShader(shader);
		if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			return shader
		}
		else {
			alert(gl.getShaderInfoLog(shader));

			return null;
		}
	}

	/** Create the program
	 * @param {Object} vertexShader Vertex shader
	 * @param {Object} fragmentShader Fragment shader
	 * @returns {Object} Program
	 */
	createProgram(vertexShader, fragmentShader) {
		var gl = this._gl;
		var program = gl.createProgram();
		gl.attachShader(program, vertexShader);
		gl.attachShader(program, fragmentShader);
		gl.linkProgram(program);
		if (gl.getProgramParameter(program, gl.LINK_STATUS)) {
			gl.useProgram(program);

			return program;
		}
		else {
			alert(gl.getProgramInfoLog(program));

			return null;
		}
	}

	/**
	 * Create the vertex buffer from data
	 * @param {Array} data Data for buffer
	 * @returns {Object} Vertex buffer object
	 */
	createBuffer(data) {
		var gl = this._gl;
		var buffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		return buffer
	}

	/**
	 * Create the index buffer from data
	 * @param {Array} data Data for buffer
	 * @returns {Object} Index buffer object
	 */
	createIndexBuffer(data) {
		var gl = this._gl;
		var buffer = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Int16Array(data), gl.STATIC_DRAW);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

		return buffer
	}

	/**
	 * Get the GL context
	 * @returns {Object} GL context
	 */
	get gl() {
		return this._gl;
	}

	/**
	 * Get the program
	 * @returns {Object} Program
	 */
	get program() {
		return this._program;
	}

	/**
	 * Set the program
	 * @param {Object} program Program
	 */
	set program(program) {
		this._program = program;
	}

	/**
	 * Get the current matrix
	 * @returns {Array} Current matrix
	 */
	get currentMatrix() {
		return this._currentMatrix;
	}

	/**
	 * Set the current matrix
	 * @param {Array} currentMatrix Current matrix
	 */
	set currentMatrix(currentMatrix) {
		this._currentMatrix = currentMatrix;
	}
}

/**
 * Class for drawables
 */
class Drawable {
	/**
	 * Draw the drawable
	 * @param {Object} context Context
	 */
	draw(context) {
		throw new Error("Not Implemented.");
	}
}

/**
 * Class for drawing logo
 */
class Logo extends Drawable {
	/**
	 * Construct the logo
	 * @constructor
	 * @param {Object} context Context
	 */
	constructor(context) {
		super();

		// Initialize vertex buffer
		var vertices = [
			-1.0, 1.0, 0.0,
			1.0, 1.0, 0.0,
			-1.0, -1.0, 0.0,
			1.0, -1.0, 0.0,
		];
		this.vertexBuffer = context.createBuffer(vertices);

		// Initialize color buffer
		var colors = [
			1.0, 0.0, 0.0, 1.0,
			0.0, 1.0, 0.0, 1.0,
			0.0, 0.0, 1.0, 1.0,
			1.0, 1.0, 1.0, 1.0,
		];
		this.colorBuffer = context.createBuffer(colors);

		// Initialize normal buffer
		var normals = [
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
		];
		this.normalBuffer = context.createBuffer(normals);

		// Initialize texture coord buffer
		var texCoords = [
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0,
			1.0, 1.0,
		];
		this.texCoordBuffer = context.createBuffer(texCoords);
	}

	/**
	 * Draw the logo
	 * @param {Object} context Context
	 */
	draw(context) {
		var gl = context.gl;
		var program = context.program;
		var attribLocation = gl.getAttribLocation(program, 'position');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'color');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 4, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'normal');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'textureCoord');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.texCoordBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 2, gl.FLOAT, false, 0, 0);
		gl.enable(gl.DEPTH_TEST);
		gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
		gl.disable(gl.DEPTH_TEST);
	}
}

/**
 * Class for drawing kaicho's head
 */
class KaichoHead extends Drawable {
	/**
	 * Construct the kaicho's head
	 * @constructor
	 * @param {Object} context Context
	 */
	constructor(context) {
		super();

		// Initialize vertex buffer
		const VERTEX_COUNT_Y = 10;
		const VERTEX_COUNT_X = 20;
		var vertices = new Array(VERTEX_COUNT_X * VERTEX_COUNT_Y * 3);
		for (var i = 0;i < VERTEX_COUNT_Y;i++) {
			var angleY = i / (VERTEX_COUNT_Y - 1) * 1.0 * Math.PI - 0.5 * Math.PI;
			var sineY = Math.sin(angleY);
			var cosineY = Math.cos(angleY);
			for (var j = 0;j < VERTEX_COUNT_X;j++) {
				var angleX = j / VERTEX_COUNT_X * 2.0 * Math.PI;
				var sineX = Math.sin(angleX);
				var cosineX = Math.cos(angleX);
				var index = (i * VERTEX_COUNT_X + j) * 3;
				vertices[index] = sineX * cosineY;
				vertices[index + 1] = sineY * 0.85;
				vertices[index + 2] = cosineX * cosineY * 0.3;
			}
		}
		this.vertexBuffer = context.createBuffer(vertices);

		// Initialize color buffer
		var colors = new Array(VERTEX_COUNT_X * VERTEX_COUNT_Y * 4);
		for (var i = 0;i < VERTEX_COUNT_Y;i++) {
			for (var j = 0;j < VERTEX_COUNT_X;j++) {
				var index = (i * VERTEX_COUNT_X + j) * 4;
				colors[index] = 1.0;
				colors[index + 1] = 1.0;
				colors[index + 2] = 1.0;
				colors[index + 3] = 1.0;
			}
		}
		this.colorBuffer = context.createBuffer(colors);

		// Initialize normal buffer
		var normals = new Array(VERTEX_COUNT_X * VERTEX_COUNT_Y * 3);
		for (var i = 0;i < VERTEX_COUNT_Y;i++) {
			for (var j = 0;j < VERTEX_COUNT_X;j++) {
				var index = (i * VERTEX_COUNT_X + j) * 3;
				var normal = getNormalizedVector([vertices[index], vertices[index + 1], vertices[index + 2]]);
				normals[index] = normal[0];
				normals[index + 1] = normal[1];
				normals[index + 2] = normal[2];
			}
		}
		this.normalBuffer = context.createBuffer(normals);

		// Initialize index buffer
		const INDEX_COUNT_X = VERTEX_COUNT_X;
		const INDEX_COUNT_Y = VERTEX_COUNT_Y - 1;
		var indices = new Array(INDEX_COUNT_X * INDEX_COUNT_Y * 6);
		for (var i = 0;i < INDEX_COUNT_Y;i++) {
			for (var j = 0;j < INDEX_COUNT_X;j++) {
				var index = (i * INDEX_COUNT_X + j) * 6;
				var jPlusOne = (j + 1) % VERTEX_COUNT_X;
				indices[index] = i * VERTEX_COUNT_X + j;
				indices[index + 1] = i * VERTEX_COUNT_X + jPlusOne;
				indices[index + 2] = (i + 1) * VERTEX_COUNT_X + j;
				indices[index + 3] = i * VERTEX_COUNT_X + jPlusOne;
				indices[index + 4] = (i + 1) * VERTEX_COUNT_X + jPlusOne;
				indices[index + 5] = (i + 1) * VERTEX_COUNT_X + j;
			}
		}
		this.indexBuffer = context.createIndexBuffer(indices);
	}

	/**
	 * Draw the kaicho's head
	 * @param {Object} context Context
	 */
	draw(context) {
		var gl = context.gl;
		var program = context.program;
		var attribLocation = gl.getAttribLocation(program, 'position');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'color');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 4, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'normal');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
		gl.enable(gl.DEPTH_TEST);
		gl.drawElements(gl.TRIANGLES, 20 * 9 * 6, gl.UNSIGNED_SHORT, 0);
		gl.disable(gl.DEPTH_TEST);
	}
}

/**
 * Class for drawing kaicho's ear
 */
class KaichoEar extends Drawable {
	/**
	 * Construct the kaicho's ear
	 * @constructor
	 * @param {Object} context Context
	 */
	constructor(context) {
		super();

		// Initialize vertex buffer and normal buffer
		const VERTEX_COUNT_X = 10;
		var vertices = new Array((VERTEX_COUNT_X + 1) * 3);
		vertices[0] = 0.0;
		vertices[1] = 0.8;
		vertices[2] = 0.0;
		for (var i = 0;i < VERTEX_COUNT_X;i++) {
			var angleX = i / VERTEX_COUNT_X * 2.0 * Math.PI;
			var sineX = Math.sin(angleX);
			var cosineX = Math.cos(angleX);
			var index = (i + 1) * 3;
			vertices[index] = sineX * 0.35;
			vertices[index + 1] = 0.0;
			vertices[index + 2] = cosineX * 0.12;
		}
		var normals = new Array((VERTEX_COUNT_X + 1) * 3);
		normals[0] = 0.0;
		normals[1] = 1.0;
		normals[2] = 0.0;
		for (var i = 0;i < VERTEX_COUNT_X;i++) {
			var index = (i + 1) * 3;
			normals[index] = vertices[index];
			normals[index + 1] = vertices[index + 1];
			normals[index + 2] = vertices[index + 2];
		}
		this.vertexBuffer = context.createBuffer(vertices);
		this.normalBuffer = context.createBuffer(normals);

		// Initialize color buffer
		var colors = new Array((VERTEX_COUNT_X + 1) * 4);
		colors[0] = 1.0;
		colors[1] = 1.0;
		colors[2] = 1.0;
		colors[3] = 1.0;
		for (var i = 0;i < VERTEX_COUNT_X;i++) {
			var index = (i + 1) * 4;
			colors[index] = 1.0;
			colors[index + 1] = 1.0;
			colors[index + 2] = 1.0;
			colors[index + 3] = 1.0;
		}
		this.colorBuffer = context.createBuffer(colors);

		// Initialize index buffer
		const EAR_INDEX_COUNT_X = VERTEX_COUNT_X;
		var indices = new Array((EAR_INDEX_COUNT_X + 1) * 3);
		for (var i = 0;i < EAR_INDEX_COUNT_X;i++) {
			var index = i * 3;
			var iPlusOne = (i + 1) % VERTEX_COUNT_X;
			indices[index] = 0;
			indices[index + 1] = i + 1;
			indices[index + 2] = iPlusOne + 1;
		}
		this.indexBuffer = context.createIndexBuffer(indices);
	}

	/**
	 * Draw the kaicho's ear
	 * @param {Object} context Context
	 */
	draw(context) {
		var gl = context.gl;
		var program = context.program;
		var attribLocation = gl.getAttribLocation(program, 'position');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'color');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 4, gl.FLOAT, false, 0, 0);
		attribLocation = gl.getAttribLocation(program, 'normal');
		gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
		gl.enableVertexAttribArray(attribLocation);
		gl.vertexAttribPointer(attribLocation, 3, gl.FLOAT, false, 0, 0);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
		gl.enable(gl.DEPTH_TEST);
		gl.drawElements(gl.TRIANGLES, 10 * 3, gl.UNSIGNED_SHORT, 0);
		gl.disable(gl.DEPTH_TEST);
	}
}

/**
 * Class for texture
 */
class Texture {
	/**
	 * Construct the texture
	 * @constructor
	 * @param {Object} context Context
	 * @param {string} source URL for source image
	 */
	constructor(context, source) {
		this.image = new Image();
		var self = this;
		this.image.onload = function() {
			var gl = context.gl;
			self.texture = gl.createTexture();
			gl.bindTexture(gl.TEXTURE_2D, self.texture);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, self.image);
			gl.generateMipmap(gl.TEXTURE_2D);
			gl.bindTexture(gl.TEXTURE_2D, null);
		};
		this.image.src = source;
	}

	/**
	 * Bind the texture
	 * @param {Object} context Context
	 */
	bind(context) {
		var gl = context.gl;
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
	}
}

/**
 * Class for storing resources
 */
class Resources {
	/**
	 * Construct the resources
	 */
	constructor() {
		this._logo = null;
		this._kaichoHead = null;
		this._kaichoEar = null;
		this._texture = null;
	}

	/**
	 * Initialize the resources
	 * @param {Object} context Context
	 */
	initialize(context) {
		this._logo = new Logo(context);
		this._kaichoHead = new KaichoHead(context);
		this._kaichoEar = new KaichoEar(context);
		this._texture = new Texture(context, "texture.png");
	}

	/**
	 * Get the logo
	 * @returns {Object} logo
	 */
	get logo() {
		return this._logo;
	}

	/**
	 * Get the kaicho head
	 * @returns {Object} kaicho head
	 */
	get kaichoHead() {
		return this._kaichoHead;
	}

	/**
	 * Get the kaicho ear
	 * @returns {Object} kaicho ear
	 */
	get kaichoEar() {
		return this._kaichoEar;
	}

	/**
	 * Get the texture
	 * @returns {Object} texture
	 */
	get texture() {
		return this._texture;
	}

	/**
	 * Get the instance
	 * @returns instance of resources
	 */
	static getInstance() {
		if (!Resources.instance) {
			Resources.instance = new Resources();
		}

		return Resources.instance;
	}
}

/**
 * Instance of resouces
 */
Resources.instance = null;

/**
 * Class for actors
 */
class Actor {
	/**
	 * Construct the actor
	 * @constructor
	 */
	constructor() {
		this.x = 0.0;
		this.y = 0.0;
		this.z = 0.0;
		this.velocityX = 0.0;
		this.velocityY = 0.0;
		this.velocityZ = 0.0;
	}

	/**
	 * Process the actor
	 */
	process() {
		throw new Error();
	}

	/**
	 * Draw the actor
	 * @param {Object} context Context
	 */
	draw(context) {
		throw new Error();
	}
}

/**
 * Class for logo actor
 */
class LogoActor extends Actor {
	/**
	 * Constructor for logo actor
	 * @constructor
	 */
	constructor() {
		super();
		this.x = Math.random() * 20.0 - 10.0;
		this.y = Math.random() * 20.0 - 10.0;
		this.z = Math.random() * -100.0;
		this.velocityX = 0.0;
		this.velocityY = 0.0;
		this.velocityZ = 0.0;
		this.angle = 0.0;
		this.angleDelta = Math.random() * 10.0 - 5.0;
	}

	/**
	 * Process the logo actor
	 */
	process() {
		this.z += 1.0;
		this.angle += this.angleDelta;
		if (this.z >= 0.0) {
			this.x = Math.random() * 20.0 - 10.0;
			this.y = Math.random() * 20.0 - 10.0;
			this.z = -100.0;
		}
	}

	/**
	 * Draw the logo actor
	 * @param {Object} context Context
	 */
	draw(context) {
		var gl = context.gl;
		var program = context.program;
		var translationMatrix = getTranslationMatrix(this.x, this.y, this.z);
		var rotationMatrix = getRotationMatrixZ(this.angle);
		var matrix = getMultipliedMatrix(translationMatrix, rotationMatrix);
		var uniformLocation = gl.getUniformLocation(program, "modelMatrix");
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(matrix));
		matrix = getMultipliedMatrix(context.currentMatrix, matrix);
		uniformLocation = gl.getUniformLocation(program, 'matrix');
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(matrix));
		Resources.getInstance().logo.draw(context);
	}
}

/**
 * Class for kaicho actor
 */
class KaichoActor extends Actor {
	/**
	 * Constructor for kaicho actor
	 * @constructor
	 */
	constructor() {
		super();
		this.x = 0.0;
		this.y = 0.0;
		this.z = -10.0;
		this.velocityX = 0.0;
		this.velocityY = 0.0;
		this.velocityZ = 0.0;
		this.angle = 0.0;
	}

	/**
	 * Process the kaicho actor
	 */
	process() {
		this.angle += 1.0;
	}

	/**
	 * Draw the kaicho actor
	 * @param {Object} context Context
	 */
	draw(context) {
		// Draw kaicho's head
		var gl = context.gl;
		var program = context.program;
		var translationMatrix = getTranslationMatrix(this.x, this.y, this.z);
		var rotationMatrix = getRotationMatrixY(this.angle);
		var headMatrix = getMultipliedMatrix(translationMatrix, rotationMatrix);
		var uniformLocation = gl.getUniformLocation(context.program, "modelMatrix");
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(headMatrix));
		var matrix = getMultipliedMatrix(context.currentMatrix, headMatrix);
		uniformLocation = gl.getUniformLocation(context.program, 'matrix');
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(matrix));
		Resources.getInstance().kaichoHead.draw(context);

		// Draw kaicho's ear
		translationMatrix = getTranslationMatrix(0.45, 0.63, 0.0);
		rotationMatrix = getRotationMatrixZ(-23.0);
		var earMatrix = getMultipliedMatrix(headMatrix, translationMatrix);
		earMatrix = getMultipliedMatrix(earMatrix, rotationMatrix);
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(earMatrix));
		matrix = getMultipliedMatrix(context.currentMatrix, earMatrix);
		uniformLocation = gl.getUniformLocation(context.program, 'matrix');
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(matrix));
		Resources.getInstance().kaichoEar.draw(context);
		translationMatrix = getTranslationMatrix(-0.45, 0.63, 0.0);
		rotationMatrix = getRotationMatrixZ(23.0);
		earMatrix = getMultipliedMatrix(headMatrix, translationMatrix);
		earMatrix = getMultipliedMatrix(earMatrix, rotationMatrix);
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(earMatrix));
		matrix = getMultipliedMatrix(context.currentMatrix, earMatrix);
		uniformLocation = gl.getUniformLocation(context.program, 'matrix');
		gl.uniformMatrix4fv(uniformLocation, false, new Float32Array(matrix));
		Resources.getInstance().kaichoEar.draw(context);
	}
}

/**
 * Class for storing scene
 */
class Scene {
	/**
	 * Construct the scene
	 * @constructor
	 */
	constructor() {
		this.logoActors = new Array();
		for (var i = 0;i < 100;i++) {
			this.logoActors.push(new LogoActor());
		}
		this.kaichoActor = new KaichoActor();
	}

	/**
	 * Process the scene
	 */
	process() {
		for (var actor of this.logoActors) {
			actor.process();
		}
		this.kaichoActor.process();
	}

	/**
	 * Draw the scene with using texture
	 * @param {Object} context Context
	 */
	drawWithTexture(context) {
		for (var actor of this.logoActors) {
			actor.draw(context);
		}
	}

	/**
	 * Draw the scene without texture
	 * @param {Object} context Context
	 */
	drawWithoutTexture(context) {
		this.kaichoActor.draw(context);
	}

	/**
	 * Get the instance
	 * @returns Instance
	 */
	static getInstance() {
		if (!Scene.instance) {
			Scene.instance = new Scene();
		}

		return Scene.instance
	}
}

/**
 *Instance of the scene
 */
Scene.instance = null;

/**
 * Function called when page is loaded
 * @namespace
 * @todo Handle keyboard and mouse
 */
onload = function() {
	// Initialize GL
	// Get the canvas
	var canvas = document.getElementById('canvas');
	canvas.width = 640;
	canvas.height = 480;

	// Get the GL context
	var gl = canvas.getContext('webgl');

	// Create the context
	var context = new Context(gl, null);

	// Initialize shaders and program
	var textureVertexShader = context.createShader('textureVertexShader');
	var noTextureVertexShader = context.createShader('noTextureVertexShader');
	var textureFragmentShader = context.createShader('textureFragmentShader');
	var noTextureFragmentShader = context.createShader('noTextureFragmentShader');
	var textureProgram = context.createProgram(textureVertexShader, textureFragmentShader);
	var noTextureProgram = context.createProgram(noTextureVertexShader, noTextureFragmentShader);
	context.program = textureProgram;

	// Create the texture
	gl.activeTexture(gl.TEXTURE0);

	// Create the resources
	var resoueces = Resources.getInstance();
	resoueces.initialize(context);

	// Create the scene
	var scene = Scene.getInstance();

	// Add event listener
	if ("ontouchend" in window) {
		canvas.addEventListener("touchmove", mouseMove, true);
	}
	else {
		canvas.addEventListener("mousemove", mouseMove, true);
	}

	// Run main loop
	var frame = 0
	mainLoop();

	return;

	/**
	 * Main loop function
	 */
	function mainLoop() {
		// Process the scene
		scene.process();

		// Clear the canvas
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT);

		// Prepare texture shader
		context.program = textureProgram;
		gl.useProgram(context.program);

		// Initialize perspective and light
		var perspectiveMatrix = getPerspectiveMatrix(30.0, 640.0 / 480.0, 0.1, 100.0);
		context.currentMatrix = perspectiveMatrix;
		var lightDirection = [
			0.0, 0.0, 1.0,
		];
		uniformLocation = gl.getUniformLocation(context.program, "lightDirection");
		gl.uniform3fv(uniformLocation, new Float32Array(lightDirection));

		// Bind the texture
		Resources.getInstance().texture.bind(context);
		uniformLocation = gl.getUniformLocation(context.program, "texture");
		gl.uniform1i(uniformLocation, 0);

		// Draw the scene with using texture
		scene.drawWithTexture(context);

		// Prepare no texture shader
		context.program = noTextureProgram;
		gl.useProgram(context.program);

		// Initialize perspective and light
		uniformLocation = gl.getUniformLocation(context.program, "lightDirection");
		gl.uniform3fv(uniformLocation, new Float32Array(lightDirection));

		// Draw the scene without texture
		scene.drawWithoutTexture(context);

		// Finalize GL
		gl.flush();
		gl.finish();

		frame++;
		setTimeout(mainLoop, 16);
	}

	function mouseMove(event) {
		var x = 0;
		var y = 0;
		if (event.changedTouches) {
			event.preventDefault();
			x = event.changedTouches[0].clientX;
			y = event.changedTouches[0].clientY;
		}
		else {
			x = event.clientX;
			y = event.clientY;
		}
		var kaichoActor = Scene.getInstance().kaichoActor;
		kaichoActor.x = (x - canvas.width * 0.5) * 0.01;
		kaichoActor.y = (y - canvas.height * 0.5) * -0.01;
	}
}
